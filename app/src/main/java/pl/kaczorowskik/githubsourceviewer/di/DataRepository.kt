package pl.kaczorowskik.githubsourceviewer.di

import io.reactivex.Observable
import pl.kaczorowskik.githubsourceviewer.mvp.model.Repository

class DataRepository(private val networkCaller: NetworkCaller) {

    private var repositories = mutableListOf<Repository>()

    fun getRepos(): Observable<List<Repository>> =
        if (repositories.isEmpty()) {
            fetchRepos()
        } else {
            Observable.just(repositories)
        }

    fun getReposByName(repoName: String): Observable<Repository> =
        getRepos()
            .flatMap { Observable.fromIterable(it) }
            .filter { it.name.contains(repoName) }


    fun getRepoById(id: String): Observable<Repository> = getRepos()
        .flatMap { Observable.fromIterable(it) }
        .filter { it.id == id }
        .takeLast(1)

    private fun fetchRepos() = networkCaller.loadRepositories()
        .map {
            repositories.clear()
            repositories.addAll(it)
            it
        }
}
