package pl.kaczorowskik.githubsourceviewer.di

import org.koin.dsl.bind
import org.koin.dsl.module
import pl.kaczorowskik.githubsourceviewer.mvp.RepositoryDetailsContract
import pl.kaczorowskik.githubsourceviewer.mvp.RepositoryListContract
import pl.kaczorowskik.githubsourceviewer.mvp.presenter.RepositoryDetailsPresenter
import pl.kaczorowskik.githubsourceviewer.mvp.presenter.RepositoryListPresenter

val appModules = module {
    single { RetrofitFactory() }
    single { NetworkCaller(get()) }
    single { DataRepository(get()) }
    single { AndroidSchedulerProvider() as SchedulerProvider }
}

val presenterModules = module {
    factory { RepositoryListPresenter(get(), get()) } bind RepositoryListContract.Presenter::class
    factory {
        RepositoryDetailsPresenter(
            get(),
            get()
        )
    } bind RepositoryDetailsContract.Presenter::class
}