package pl.kaczorowskik.githubsourceviewer.di

import pl.kaczorowskik.githubsourceviewer.network.GithubService

class NetworkCaller(private val retrofitFactory: RetrofitFactory) {
    private val githubService by lazy { retrofitFactory.createService(GithubService::class.java) }

    fun loadRepositories() = githubService.getRepos()
}