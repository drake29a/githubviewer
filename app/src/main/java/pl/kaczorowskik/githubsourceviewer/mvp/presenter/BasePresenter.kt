package pl.kaczorowskik.githubsourceviewer.mvp.presenter

import io.reactivex.disposables.CompositeDisposable
import pl.kaczorowskik.githubsourceviewer.di.SchedulerProvider
import pl.kaczorowskik.githubsourceviewer.mvp.BaseContract

abstract class BasePresenter<T : BaseContract.View>(protected val schedulerProvider: SchedulerProvider) :
    BaseContract.Presenter<T> {

    protected lateinit var view: T
    protected lateinit var compositeDisposable: CompositeDisposable

    final override fun start(view: T) {
        this.view = view
        compositeDisposable = CompositeDisposable()
        onStart()
    }

    final override fun stop() {
        onStop()
        compositeDisposable.dispose()
    }

    open fun onStart() {}

    open fun onStop() {}
}