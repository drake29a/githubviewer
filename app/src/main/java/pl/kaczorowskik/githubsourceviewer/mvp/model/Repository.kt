package pl.kaczorowskik.githubsourceviewer.mvp.model

import com.squareup.moshi.Json

data class Repository(
    @field:Json(name = "id") val id: String,
    @field:Json(name = "node_id") val node_id: String,
    @field:Json(name = "name") val name: String,
    @field:Json(name = "full_name") val full_name: String,
    @field:Json(name = "private") val private: Boolean,
    @field:Json(name = "created_at") val created_at: String,
    @field:Json(name = "updated_at") val updated_at: String,
    @field:Json(name = "pushed_at") val pushed_at: String,
    @field:Json(name = "git_url") val git_url: String,
    @field:Json(name = "owner") val owner: Owner,
    @field:Json(name = "stargazers_count") val stargazers_count: Int,
    @field:Json(name = "size") val size: Int,
    @field:Json(name = "description") val description: String
)