package pl.kaczorowskik.githubsourceviewer.mvp.view

import android.os.Bundle
import android.view.Menu
import android.view.MenuInflater
import android.view.View
import androidx.appcompat.widget.SearchView
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.jakewharton.rxbinding3.appcompat.queryTextChanges
import kotlinx.android.synthetic.main.repository_list.*
import org.koin.android.ext.android.inject
import pl.kaczorowskik.githubsourceviewer.R
import pl.kaczorowskik.githubsourceviewer.mvp.RepositoryListContract
import pl.kaczorowskik.githubsourceviewer.utils.disposeWith


class RepositoryList :
    BaseFragment<RepositoryListContract.View, RepositoryListContract.Presenter>(R.layout.repository_list),
    RepositoryListContract.View {

    override val presenter: RepositoryListContract.Presenter by inject()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        itemList.adapter = RepositoryItemListAdapter(presenter)
        itemList.layoutManager = LinearLayoutManager(context)
    }

    override fun repositoryListReady() {
        itemList.adapter?.notifyDataSetChanged()
    }

    override fun goToRepositoryDetails(id: String) {
        val action = RepositoryListDirections.actionRepositoryListToRepositoryDetails(id)
        findNavController().navigate(action)
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, inflater)
        inflater.inflate(R.menu.repository_list_menu, menu)
        val searchView =
            menu.findItem(R.id.action_search).actionView as? SearchView
        searchView?.queryTextChanges()?.subscribe {
            presenter.filterItems(it.toString())
        }?.disposeWith(compositeDisposable)
    }
}
