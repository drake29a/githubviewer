package pl.kaczorowskik.githubsourceviewer.mvp.view

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.jakewharton.rxbinding3.view.clicks
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.repository_list_item.view.*
import pl.kaczorowskik.githubsourceviewer.R
import pl.kaczorowskik.githubsourceviewer.mvp.RepositoryListContract

class RepositoryItemListAdapter(var presenter: RepositoryListContract.Presenter) :
    RecyclerView.Adapter<RepositoryItemListAdapter.EntryViewHolder>() {
    override fun getItemCount() = presenter.getItemsCount()

    override fun onBindViewHolder(holder: EntryViewHolder, position: Int) =
        presenter.onBindEntity(holder, position)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): EntryViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.repository_list_item, parent, false)
        return EntryViewHolder(view)
    }

    class EntryViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView),
        RepositoryListContract.EntityView {

        override fun clicks() = itemView.clicks()

        override fun setName(name: String) {
            itemView.name.text = name
        }

        override fun setStargazersCount(count: Int) {
            itemView.stargazersCount.text = count.toString()
        }

        override fun setIcon(url: String?) {
            url?.let {
                Picasso.with(itemView.context).load(url).into(itemView.ownerLogo)
            }
        }
    }
}