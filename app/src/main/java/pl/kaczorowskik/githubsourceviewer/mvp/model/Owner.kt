package pl.kaczorowskik.githubsourceviewer.mvp.model

import com.squareup.moshi.Json


data class Owner(
    @field:Json(name = "login") val login: String,
    @field:Json(name = "id") val id: String,
    @field:Json(name = "node_id") val node_id: String,
    @field:Json(name = "avatar_url") val avatar_url: String,
    @field:Json(name = "html_url") val html_url: String
)
