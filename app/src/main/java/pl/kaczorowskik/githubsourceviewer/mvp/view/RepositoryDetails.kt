package pl.kaczorowskik.githubsourceviewer.mvp.view

import android.os.Bundle
import android.view.View
import androidx.navigation.fragment.navArgs
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.repository_details_owner.*
import kotlinx.android.synthetic.main.repository_details_repository_info.*
import org.koin.android.ext.android.inject
import pl.kaczorowskik.githubsourceviewer.R
import pl.kaczorowskik.githubsourceviewer.mvp.RepositoryDetailsContract

class RepositoryDetails :
    BaseFragment<RepositoryDetailsContract.View, RepositoryDetailsContract.Presenter>(R.layout.repository_details),
    RepositoryDetailsContract.View {

    override val presenter: RepositoryDetailsContract.Presenter by inject()
    private val repositoryDetailsArgs by navArgs<RepositoryDetailsArgs>()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        presenter.loadRepository(repositoryDetailsArgs.id)
    }

    override fun setOwnerLogo(url: String?) {
        url?.let {
            Picasso.with(context).load(it).into(ownerLogo)
        }
    }

    override fun setOwnerName(name: String) {
        ownerName.text = name
    }

    override fun setOwnerUrl(url: String?) {
        ownerUrl.text = url
    }

    override fun setRepositorySize(size: Int) {
        sizeCount.text = size.toString()
    }

    override fun setRepositoryStargazers(stargazers: Int) {
        stargazersCount.text = stargazers.toString()
    }

    override fun setRepostioryName(name: String) {
        repositoryName.text = name
    }

    override fun setRepositoryDescription(description: String?) {
        repositoryDescription.text = description
    }


}