package pl.kaczorowskik.githubsourceviewer.mvp.presenter

import pl.kaczorowskik.githubsourceviewer.di.DataRepository
import pl.kaczorowskik.githubsourceviewer.di.SchedulerProvider
import pl.kaczorowskik.githubsourceviewer.mvp.RepositoryListContract
import pl.kaczorowskik.githubsourceviewer.mvp.model.Repository
import pl.kaczorowskik.githubsourceviewer.utils.disposeWith
import kotlin.math.min

class RepositoryListPresenter(
    private val dataRepository: DataRepository,
    schedulerProvider: SchedulerProvider
) :
    BasePresenter<RepositoryListContract.View>(schedulerProvider),
    RepositoryListContract.Presenter {

    private var repositoriesList: List<Repository>? = null

    override fun filterItems(filter: String) {
        dataRepository.getReposByName(filter)
            .toList()
            .toObservable()
            .subscribeOn(schedulerProvider.io())
            .observeOn(schedulerProvider.ui())
            .subscribe {
                repositoriesList = it
                view.repositoryListReady()
            }
            .disposeWith(compositeDisposable)
    }

    override fun loadItems() {
        dataRepository.getRepos()
            .subscribeOn(schedulerProvider.io())
            .observeOn(schedulerProvider.ui())
            .subscribe {
                repositoriesList = it
                view.repositoryListReady()
            }
            .disposeWith(compositeDisposable)
    }

    override fun onStart() {
        loadItems()
    }

    override fun onBindEntity(entityView: RepositoryListContract.EntityView, position: Int) {
        repositoriesList?.get(position)?.let {
            entityView.setIcon(it.owner.avatar_url)
            entityView.setName(it.name)
            entityView.setStargazersCount(it.stargazers_count)
            entityView.clicks()
                .subscribe { _ -> view.goToRepositoryDetails(it.id) }
                .disposeWith(compositeDisposable)
        }
    }

    override fun getItemsCount() = min(repositoriesList?.size ?: 0, 10)
}