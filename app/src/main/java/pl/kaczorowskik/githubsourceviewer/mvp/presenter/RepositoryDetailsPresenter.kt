package pl.kaczorowskik.githubsourceviewer.mvp.presenter

import pl.kaczorowskik.githubsourceviewer.di.DataRepository
import pl.kaczorowskik.githubsourceviewer.di.SchedulerProvider
import pl.kaczorowskik.githubsourceviewer.mvp.RepositoryDetailsContract
import pl.kaczorowskik.githubsourceviewer.mvp.model.Repository
import pl.kaczorowskik.githubsourceviewer.utils.disposeWith

class RepositoryDetailsPresenter(
    private val dataRepository: DataRepository,
    schedulerProvider: SchedulerProvider
) :
    BasePresenter<RepositoryDetailsContract.View>(schedulerProvider),
    RepositoryDetailsContract.Presenter {

    override fun loadRepository(id: String) {
        dataRepository.getRepoById(id)
            .subscribeOn(schedulerProvider.io())
            .observeOn(schedulerProvider.ui())
            .subscribe { displayRepositoryDetails(it) }
            .disposeWith(compositeDisposable)
    }

    private fun displayRepositoryDetails(it: Repository) {
        with(it.owner) {
            view.setOwnerLogo(avatar_url)
            view.setOwnerName(login)
            view.setOwnerUrl(html_url)
        }

        with(it) {
            view.setRepositorySize(size)
            view.setRepositoryStargazers(stargazers_count)
            view.setRepostioryName(name)
            view.setRepositoryDescription(description)
        }
    }
}