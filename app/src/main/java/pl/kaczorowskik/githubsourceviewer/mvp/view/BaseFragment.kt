package pl.kaczorowskik.githubsourceviewer.mvp.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import io.reactivex.disposables.CompositeDisposable
import pl.kaczorowskik.githubsourceviewer.mvp.BaseContract

abstract class BaseFragment<V : BaseContract.View, P : BaseContract.Presenter<V>>(private val layout: Int) :
    Fragment(), BaseContract.View {

    protected abstract val presenter: P
    protected lateinit var compositeDisposable: CompositeDisposable private set

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(layout, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        compositeDisposable = CompositeDisposable()
        presenter.start(this as V)
    }

    override fun onDestroyView() {
        super.onDestroyView()
        compositeDisposable.dispose()
    }
}