package pl.kaczorowskik.githubsourceviewer.mvp

interface BaseContract {

    interface Presenter<T : View> {
        fun start(view: T)
        fun stop()
    }

    interface View
}
