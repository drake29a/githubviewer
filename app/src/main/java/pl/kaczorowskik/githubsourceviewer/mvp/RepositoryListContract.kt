package pl.kaczorowskik.githubsourceviewer.mvp

import io.reactivex.Observable

interface RepositoryListContract {
    interface View : BaseContract.View {
        fun repositoryListReady()
        fun goToRepositoryDetails(id: String)
    }

    interface Presenter : BaseContract.Presenter<View> {
        fun loadItems()
        fun filterItems(filter: String)
        fun onBindEntity(entityView: EntityView, position: Int)
        fun getItemsCount(): Int
    }

    interface EntityView {
        fun setName(name: String)
        fun setStargazersCount(count: Int)
        fun setIcon(url: String?)
        fun clicks(): Observable<Unit>

    }
}