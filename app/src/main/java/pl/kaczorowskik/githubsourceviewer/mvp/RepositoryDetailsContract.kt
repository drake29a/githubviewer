package pl.kaczorowskik.githubsourceviewer.mvp

interface RepositoryDetailsContract {
    interface View : BaseContract.View {
        fun setOwnerLogo(url: String?)
        fun setOwnerName(name: String)
        fun setOwnerUrl(url: String?)
        fun setRepositorySize(size: Int)
        fun setRepositoryDescription(description: String?)
        fun setRepositoryStargazers(stargazers: Int)
        fun setRepostioryName(name: String)
    }

    interface Presenter : BaseContract.Presenter<View> {
        fun loadRepository(id: String)
    }
}