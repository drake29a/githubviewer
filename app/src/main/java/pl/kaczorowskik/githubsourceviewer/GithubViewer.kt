package pl.kaczorowskik.githubsourceviewer

import android.app.Application
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin
import pl.kaczorowskik.githubsourceviewer.di.appModules
import pl.kaczorowskik.githubsourceviewer.di.presenterModules

class GithubViewer : Application() {

    override fun onCreate() {
        super.onCreate()
        startKoin {
            androidContext(this@GithubViewer)
            modules(listOf(appModules, presenterModules))
        }
    }
}
