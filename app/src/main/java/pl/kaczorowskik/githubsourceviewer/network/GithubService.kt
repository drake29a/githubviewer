package pl.kaczorowskik.githubsourceviewer.network

import io.reactivex.Observable
import pl.kaczorowskik.githubsourceviewer.mvp.model.Repository
import retrofit2.http.GET

interface GithubService {
    @GET("orgs/applauseoss/repos")
    fun getRepos(): Observable<List<Repository>>
}